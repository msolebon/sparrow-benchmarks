#include<sparrow.h>
#include<stdio.h>

int check(int valid, int res, const char * func)
{
    if (valid == res) return 0;
    printf("%s: expected 0x%08x, got 0x%08x\n", func, valid, res);
    return 1;
}

int main()
{
    int8x8_t a,b,c,r;
    int errors = 0;

    //test add
    a = 0x8180ff01;
    b = 0x81ff7f7f;
    r = 0x027f7e80;
    c = vadd_s8(a,b);
    errors += check(r, c, "add");

    //test saturated add
    a = 0x8180ff01;
    b = 0x81ff7f7f;
    r = 0x80807e7f;
    c = vaddq_s8(a,b);
    errors += check(r, c, "saturated add");

    //test sub
    a = 0x807f0afb;
    b = 0x05fffb0a;
    r = 0x7b800ff1;
    c = vsub_s8(a,b);
    errors += check(r, c, "sub");

    //test saturated sub
    a = 0x807f0afb;
    b = 0x05fffb0a;
    r = 0x807f0ff1;
    c = vsubq_s8(a,b);
    errors += check(r, c, "saturated sub");

    //test Max MAX signed
    a = 0x0204080a;
    b = 0x204080a0;
    r = 0x00000040;
    c = max_s8(a,b);
    errors += check(r, c, "signed maximum");

    //test Max MAX unsigned
    r = 0x000000a0;
    c = max_u8(a,b);
    errors += check(r, c, "unsigned maximum");

    //test Min MIN signed
    r = 0xffffff80;
    c = min_s8(a,b);
    errors += check(r, c, "signed minimum");

    //test Min MIN unsigned
    r = 0x00000002;
    c = min_u8(a,b);
    errors += check(r, c, "unsigned minimum");

    //test dot product
    a = 0xfffe03fc;
    b = 0x00ff0203;
    r = 0xfffffffc;
    c = dot_s8(a,b);
    errors += check(r, c, "dot product");

    //test saturated mul
    a = 0x7f7ffdff;
    b = 0x7fff7f80;
    r = 0x7f81807f;
    c = vmulq_s8(a,b);
    errors += check(r, c, "saturated multiplication");

    //test nand as not (a nand a = !a)
    a = 0xdeadbeaf;
    r = 0x21524150;
    c = vnand_u8(a,a);
    errors += check(r, c, "nand as not");

    //test xor reduction
    a = 0xfeedcafe;
    r = 0x00000027;
    __stage2(c, "xor", a);
    errors += check(r, c, "xor reduction");

    printf("Execution ended with %d errors\n", errors);
    puts("END OF TEST");

}
