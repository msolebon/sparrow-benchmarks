#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <vortex.h>
#include "kernel.h"

#define KERNEL_ARG_DEV_MEM_ADDR 0x5ffff000

int main()
{

    uint64_t num_points = 256;
    uint32_t *input = (uint32_t*) malloc(num_points * sizeof(uint32_t));
    for (int i = 0; i < num_points; ++i) input[i] = i;
    vx_device_h device;
    puts("Start connection with the GPU");
    vx_dev_open(&device);
    puts("Retrieving GPU configuration:");
    uint64_t nwarps, nthreads, ncores;
    vx_dev_caps(device, VX_CAPS_NUM_THREADS, &nthreads);
    vx_dev_caps(device, VX_CAPS_NUM_WARPS, &nwarps);
    vx_dev_caps(device, VX_CAPS_NUM_CORES, &ncores);
    printf("    - %ld cores\n    - %ld warps\n    - %ld threads\n", ncores, nwarps, nthreads);
    puts("Upload kernel to the device");
    vx_upload_kernel_bytes(device, build_kernel_bin, build_kernel_bin_len);
    puts("Allocate device memory");
    uint64_t src, dest;
    vx_mem_alloc(device, num_points * sizeof(uint32_t), VX_MEM_TYPE_GLOBAL, &src);
    vx_mem_alloc(device, num_points * sizeof(uint32_t), VX_MEM_TYPE_GLOBAL, &dest);
    puts("Upload kernel arguments");
    uint64_t arg[3] = {num_points, src, dest};
    vx_copy_to_dev(device, KERNEL_ARG_DEV_MEM_ADDR, arg, 3*sizeof(uint64_t));
    puts("Send input data to the device");
    vx_copy_to_dev(device, src, input, num_points * sizeof(uint32_t));
    puts("Start execution");
    vx_start(device);
    puts("Waiting for execution to complete");
    vx_ready_wait(device, VX_MAX_TIMEOUT);
    puts("Retrieving data from the device");
    uint32_t *output = (uint32_t*) malloc(num_points * sizeof(uint32_t));
    vx_copy_from_dev(device, output, dest, num_points * sizeof(uint32_t));
    puts("Free allocated memory");
    vx_mem_free(device, src);
    vx_mem_free(device, dest);
    puts("Close connection with the GPU");
    vx_dev_close(device);
    for (int i = 0; i < num_points; ++i) if(output[i] != 2*i) { printf("Test failed at (%d), got %d",i,output[i]); exit(0);}
    puts("Test passed!");
}
